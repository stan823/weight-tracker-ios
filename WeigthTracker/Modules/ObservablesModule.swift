//
//  ObservablesModule.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 01/04/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
import RxSwift

class ObservablesModule {
    
    static let instance = ObservablesModule()
    
    private init(){}
    
    let service:ObservablesService = ObservablesServiceImpl()
    
}

protocol ObservablesService{
    func provideWeightUnitObservable() -> Variable<WeightUnit>
    func provideCurrentlyEnteredWeightKg() -> Variable<Float>
}

class ObservablesServiceImpl:ObservablesService{
    
    private let weightUnitObservable = Variable<WeightUnit>(WeightUnit.kg)
    private let currentlyEnteredWeightKg = Variable<Float>(0.0)
    
    func provideWeightUnitObservable() -> Variable<WeightUnit> {
        return weightUnitObservable;
    }
    
    func provideCurrentlyEnteredWeightKg() -> Variable<Float>{
        return currentlyEnteredWeightKg
    }
}
