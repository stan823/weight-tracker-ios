//
//  WeightUnit.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 25/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation

enum WeightUnit{
    case kg
    case lb
    
    static func from(index:Int) -> WeightUnit {
        switch index {
        case 0:
            return WeightUnit.kg
        default:
            return WeightUnit.lb
        }
    }
}


