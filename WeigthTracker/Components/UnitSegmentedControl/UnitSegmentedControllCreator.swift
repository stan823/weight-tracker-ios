//
//  UnitSegmentedControllCreator.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 01/04/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation

class UnitSegmentedControllCreator{
    
    static let inctance = UnitSegmentedControllCreator()
    
    private init(){}
    
    func createInteractor() -> UnitSegmentedControlInteractor{
        return UnitSegmentedControlInteractorImpl(
            observablesService: ObservablesModule.instance.service
        )
    }
}
