//
//  UnitSegmentedControlInteractorImpl.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 25/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
import RxSwift

class UnitSegmentedControlInteractorImpl:UnitSegmentedControlInteractor{
    
    private let observablesService:ObservablesService
    
    init(observablesService:ObservablesService) {
        self.observablesService = observablesService
    }
    
    func onUnitIndexChanged(index: Int) {
       
        let unit = WeightUnit.from(index: index)
        PrintUtil.printWeightField("Setting \(unit)")
        observablesService.provideWeightUnitObservable().value = unit
    }
}
