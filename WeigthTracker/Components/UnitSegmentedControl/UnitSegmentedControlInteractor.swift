//
//  UnitSegmentedControlInteractor.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 25/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation

protocol UnitSegmentedControlInteractor {
    
    func onUnitIndexChanged(index:Int)
}
