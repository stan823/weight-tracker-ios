//
//  UnitSegmentedControll.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 25/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import UIKit
import Foundation

class UnitSegmentedControll: UISegmentedControl {
    
    let interactor:UnitSegmentedControlInteractor = UnitSegmentedControllCreator.inctance.createInteractor()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addTarget(self, action: #selector(onValueChanged(sender:)), for: .valueChanged)
    }
    
    
    @objc func onValueChanged(sender:UISegmentedControl){
        PrintUtil.printWeightField("hello")
        interactor.onUnitIndexChanged(index: sender.selectedSegmentIndex)
    }

}
