//
//  WeightFieldKilogramInteractor.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 18/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation


protocol WeightFieldKilogramInteractor{
    func onTextChanged(_ text:String?)
}
