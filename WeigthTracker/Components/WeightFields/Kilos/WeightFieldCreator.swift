//
//  WeightFieldCreator.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 01/04/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation

class WeightFieldCreator{
    
    static let instance = WeightFieldCreator()
    
    private init(){}
    
    func createPresenter() -> WeightFeildKilogramPresenter  {
        return WeightFeildKilogramPresenterImpl(
            observablesService: ObservablesModule.instance.service)
    }
    
    func createInteractor() -> WeightFieldKilogramInteractor {
        return WeightFieldKilogramInteractorImpl(observableService: ObservablesModule.instance.service)
    }
    
}
