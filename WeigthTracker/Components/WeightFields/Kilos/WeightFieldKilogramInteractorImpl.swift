//
//  WeightFieldKilogramInteractorImpl.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 18/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
import RxSwift

class WeightFieldKilogramInteractorImpl:WeightFieldKilogramInteractor{
    
    let observableService:ObservablesService
    
    init (observableService:ObservablesService){
        self.observableService = observableService
    }
    
    func onTextChanged(_ text: String?) {
        let value = Float(text ?? "0.0")
        let variable = observableService.provideCurrentlyEnteredWeightKg()
        
        if let value = value {
           variable.value = value
        }
    }
}
