//
//  WeightFieldKilogramPresenter.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 25/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
import RxSwift

protocol WeightFeildKilogramPresenter {
    
    func observeWeightUnit(_ observer: @escaping ((WeightUnit) -> Void))
    func removeWeightUnitObserver()
}
