//
//  WeightFieldKilogram.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 18/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import UIKit

class WeightFieldKilogram: UITextField {
    
    let interactor = WeightFieldCreator.instance.createInteractor()
    let presenter = WeightFieldCreator.instance.createPresenter()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.placeholder = Strings.kgsShort.localize()
        
        addTarget(self, action: #selector(textChanged), for: .editingChanged)
        presenter.observeWeightUnit(self.unitObserver)
    }
    
    deinit {
        PrintUtil.printWeightField("weight field removed");
        presenter.removeWeightUnitObserver()
        
    }
    
    func unitObserver(_ unit:WeightUnit){
        PrintUtil.printWeightField("received \(unit)")
        DispatchQueue.main.async {
            
            if unit == WeightUnit.kg{
               self.isHidden = false
            }else{
                self.isHidden = true
            }
        }
    }
    
    @objc func textChanged(_ textField:UITextField){
        interactor.onTextChanged(self.text)
    }
    
}
