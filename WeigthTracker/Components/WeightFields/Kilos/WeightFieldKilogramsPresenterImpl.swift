//
//  WeightFieldKilogramsPresenterImpl.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 25/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
import RxSwift

class WeightFeildKilogramPresenterImpl :WeightFeildKilogramPresenter{
    
    private let observablesService:ObservablesService
    private var disposable:Disposable?
    
    init(observablesService:ObservablesService) {
        self.observablesService = observablesService
    }
   
    func observeWeightUnit(_ observer: @escaping ((WeightUnit) -> Void)) {
        let weightUnitObservable = observablesService.provideWeightUnitObservable()
        self.disposable = weightUnitObservable
            .asObservable()
            .subscribe(onNext: observer, onError: nil, onCompleted: nil, onDisposed: nil)
    }
    
    func removeWeightUnitObserver() {
        self.disposable?.dispose()
    }
    
    
}
