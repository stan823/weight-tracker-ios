//
//  WeightFieldPounds.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 01/04/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import UIKit

class WeightFieldPounds: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.placeholder = Strings.lbsShort.localize()
    }

}
