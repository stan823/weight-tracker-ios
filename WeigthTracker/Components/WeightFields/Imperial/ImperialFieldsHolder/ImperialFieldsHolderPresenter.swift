//
//  ImperialFieldsHolderPresenter.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 28/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation

protocol ImperialFieldsHolderPresenter {
    
    func observeVisibility(_ observer: @escaping (Bool)->Void)
    func removeVisiblityOBserver()
}
