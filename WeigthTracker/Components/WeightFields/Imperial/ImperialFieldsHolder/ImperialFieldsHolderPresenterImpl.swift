//
//  ImperialFieldsHolderPresenterImpl.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 28/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
import RxSwift

class ImperialFieldsHolderPresenterImpl :ImperialFieldsHolderPresenter{
    
    private let observablesService:ObservablesService
    private var disposable:Disposable?
    
    init(observablesService:ObservablesService) {
        self.observablesService = observablesService
    }
    
    func observeVisibility(_ observer: @escaping (Bool) -> Void) {
        disposable = observablesService.provideWeightUnitObservable()
            .asObservable()
            .map(self.visibilityMapper)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: observer, onError: nil, onCompleted: nil, onDisposed: nil)
    }
    
    func removeVisiblityOBserver() {
        disposable?.dispose()
    }
    
    private func visibilityMapper(weightUnit:WeightUnit)->Bool{
        return weightUnit == .lb
    }
    
    
}
