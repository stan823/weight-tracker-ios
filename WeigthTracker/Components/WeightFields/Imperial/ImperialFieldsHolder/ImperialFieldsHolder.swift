//
//  ImperialFieldsHolder.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 28/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import UIKit

class ImperialFieldsHolder: UIStackView {

    let presenter = ImperialFieldsHolderCreator.instance.createPresenter()
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        presenter.observeVisibility(self.visibilityObserver)
    }
    
    deinit {
        presenter.removeVisiblityOBserver()
    }
    
    private func visibilityObserver(visible:Bool){
        DispatchQueue.main.async {
         
       self.isHidden = !visible
            
        }
    }

}
