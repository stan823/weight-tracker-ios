//
//  ImperialFieldsHolderCreator.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 01/04/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation

class ImperialFieldsHolderCreator{
    
    static let instance = ImperialFieldsHolderCreator()
    
    private init(){}
    
    func createPresenter() -> ImperialFieldsHolderPresenter{
        return ImperialFieldsHolderPresenterImpl(
            observablesService: ObservablesModule.instance.service
        )
    }
}
