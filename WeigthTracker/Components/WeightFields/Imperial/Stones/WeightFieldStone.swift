//
//  WeightFieldStone.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 30/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import UIKit

class WeightFieldStone: UITextField {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        placeholder = Strings.stonesShort.localize()

    }
   

}
