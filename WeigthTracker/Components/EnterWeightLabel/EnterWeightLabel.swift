//
//  EnterWeightLabel.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 28/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import UIKit

class EnterWeightLabel: UILabel {
    
    private let presenter = EnterWeightLabelCreator.instance.createPresenter()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        presenter.observeWeightUnit(observer: onWeightUnitChanged)
    }
    
    private func onWeightUnitChanged(text:String){
        self.text = text
    }
    
    deinit {
        presenter.removeObserver()
    }

}
