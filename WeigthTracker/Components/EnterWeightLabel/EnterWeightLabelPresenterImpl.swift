//
//  EnterWeightLabelPresenterImpl.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 28/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
import RxSwift

class EnterWeightLabelPresenterImpl:EnterWeightLabelPresenter {
    
    let observablesService:ObservablesService
    var disposable:Disposable? = nil
    
    init(observablesService:ObservablesService) {
        self.observablesService = observablesService
    }
    
    func observeWeightUnit(observer: @escaping (String) -> Void) {
        disposable = observablesService.provideWeightUnitObservable().asObservable()
            .map{ (weightUnit:WeightUnit) -> String in
                if weightUnit == WeightUnit.kg {
                    return Strings.enterWeightKg.localize()
                }else{
                    return Strings.enterWeightLbs.localize()
                }
            }.subscribe(onNext: observer, onError: nil, onCompleted: nil, onDisposed: nil)
    }
    
    func removeObserver() {
        self.disposable?.dispose()
    }
    
    
}
