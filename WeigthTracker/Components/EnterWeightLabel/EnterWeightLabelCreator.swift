//
//  EnterWeightLabelCreator.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 01/04/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation

class EnterWeightLabelCreator{
    
    static let instance = EnterWeightLabelCreator()
    
    private init(){}
    
    func createPresenter() -> EnterWeightLabelPresenter{
        return EnterWeightLabelPresenterImpl(
            observablesService: ObservablesModule.instance.service
        )
    }
}
