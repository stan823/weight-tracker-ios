//
//  SaveButton.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 11/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import UIKit

class SaveButton: UIButton {
    
    let interactor:SaveButtonInteractor = SaveButtonCreator.instance.createInteractor()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addTarget(self, action: #selector(onClick), for: .touchUpInside)
    }
    
    @objc func onClick(_ sender:UIButton){
        interactor.onClick()
    }

}
