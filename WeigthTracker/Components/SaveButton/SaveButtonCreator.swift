//
//  SaveButtonCreator.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 01/04/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation

class SaveButtonCreator{
    
    static let instance = SaveButtonCreator()
    
    private init(){}
    
    func createInteractor() -> SaveButtonInteractor{
        return SaveButtonInteractorImpl(
            storageService: StorageModule.instance.service,
            timeService: TimeModule.instance.service,
            observablesService: ObservablesModule.instance.service
        )
    }
}
