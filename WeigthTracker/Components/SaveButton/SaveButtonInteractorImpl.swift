//
//  SaveButtonInteractorImpl.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 11/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
import RxSwift

class SaveButtonInteractorImpl: SaveButtonInteractor {
    
    let storageService:StorageService
    let timeService:TimeService
    let observablesService:ObservablesService
    
    init(
        storageService:StorageService,
        timeService:TimeService,
        observablesService:ObservablesService
        )
    {
        self.storageService = storageService
        self.timeService = timeService
        self.observablesService = observablesService
    }
    
    func onClick() {
        storageService.saveWeight(
            kgs: observablesService.provideCurrentlyEnteredWeightKg().value,
            timestamp: timeService.provideCurrentTimestamp()
        )
    }
}
