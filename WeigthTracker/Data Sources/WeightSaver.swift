//
//  WeightStorer.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 11/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation

protocol WeightSaver {
    func saveWeight(weightKg:Float, timeStamp:Int)
}
