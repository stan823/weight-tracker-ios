//
//  PrintUtil.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 25/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation

class PrintUtil{
    
    enum PrintTag{
        case weightField
    }
    
    static var tag = PrintTag.weightField
    
    static func printWeightField(_ msg:String){
        printTag(tag: .weightField, msg: msg)
    }
    
    private static func printTag(tag: PrintTag, msg:String){
        if tag == PrintUtil.tag {
            print(msg)
        }
    }
}
