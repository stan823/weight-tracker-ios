//
//  TimeService.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 01/04/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation

protocol TimeService {
    func provideCurrentTimestamp()->Int
}
