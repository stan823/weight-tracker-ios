//
//  Strings.swift
//  WeigthTracker
//
//  Created by Stanislaw Brzezinski on 25/03/2019.
//  Copyright © 2019 Stanislaw Brzezinski. All rights reserved.
//

import Foundation

enum Strings : String{
    case enterWeightKg = "EnterWeightKgs"
    case enterWeightLbs = "EnterWeightLbs"
    case kgsShort =  "kgs_short"
    case lbsShort = "lbs_short"
    case stonesShort = "stones_short"
    
    func localize() -> String{
        return NSLocalizedString(self.rawValue, comment: "")
    }
}
